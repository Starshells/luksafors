//
//  ViewController.swift
//  Luksofors
//
//  Created by students on 15/02/2018.
//  Copyright © 2018 students. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myDatePicker: UIDatePicker!
    @IBOutlet weak var Aplis: UIView!
    
    @IBOutlet weak var selectedDate: UILabel!
    let Balts = [UIColor.white.cgColor, UIColor.white.cgColor]
    let Sarkans = [UIColor.red.cgColor, UIColor.red.cgColor]
    let Zals = [UIColor.green.cgColor, UIColor.green.cgColor]
    let Dzeltens = [UIColor.yellow.cgColor, UIColor.yellow.cgColor]
    let gradient = CAGradientLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: Aplis.frame.size.width, height: Aplis.frame.size.width))
        gradient.frame = view.bounds
        gradient.colors = Sarkans
        Aplis.layer.insertSublayer(gradient, at: 0)
        Aplis.layer.cornerRadius = Aplis.frame.size.width/2
        Aplis.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    @IBAction func pulkstenis(_ sender: Any) {

        let date = myDatePicker.date
        let components = Calendar.current.dateComponents([.hour, .minute], from: date)
        let hour = components.hour!
        let minute = components.minute!
        let counter = ((hour - 6) * 60 + minute) % 7
        if (hour < 6){
            gradient.colors = Balts}
        else {
            switch counter{
            case 1, 2:
                gradient.colors = Zals
            case 4, 5, 6:
                gradient.colors = Sarkans
            default:
                gradient.colors = Dzeltens
            }
            Aplis.layer.insertSublayer(gradient, at:0)
            }
            
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 190,y: 430), radius: CGFloat(120), startAngle: CGFloat(0), endAngle:CGFloat(Double.pi * 2), clockwise: true)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath

        }
            
    }
    


